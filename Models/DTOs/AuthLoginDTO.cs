using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class AuthLoginDTO {
    public string Username { get; set; }
    public string Password { get; set; }
  }
}
