# DTOs (Data Transfer Object)

Namespace responsável pelos DTOs, que são objetos de transferência de dados,
comumente usado para envio de dados para uma endpoint sem a nescessidade
de passar dados inuteis, como `CreatedAt`, `UpdatedAt` e `ID`, que são dados
gerados automaticamente.
