using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class SaleCreateDTO {
    public ICollection<int> BuyPacksIDs { get; set; }
    public ICollection<int> BuyProductsIDs { get; set; }

    public int UserID { get; set; }

    public int ClientID { get; set; }

    public double Price { get; set; }
  }
}
