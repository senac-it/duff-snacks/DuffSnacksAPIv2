using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class ClientUpdateDTO {
    public string? Fullname { get; set; }

    public string? Email { get; set; }

    public string? Document { get; set; }

    public string? Address { get; set; }
  }
}
