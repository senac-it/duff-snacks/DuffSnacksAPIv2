using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class PackCreateDTO {
    public string? Name { get; set; }

    public string? Description { get; set; }

    public bool Disabled { get; set; }

    public double Price { get; set; }

    public ICollection<PackAssocAssociateDTO> PacksAssocs { get; set; }
  }
}
