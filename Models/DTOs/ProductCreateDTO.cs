using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class ProductCreateDTO {
    public string? Name { get; set; }

    public string? Description { get; set; }

    public double Price { get; set; }

    public double Discount { get; set; }

    public bool DiscountIsPercentage { get; set; }

    public int CategoryID { get; set; }
  }
}
