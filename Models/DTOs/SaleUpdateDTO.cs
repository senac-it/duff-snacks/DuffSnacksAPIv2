using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class SaleUpdateDTO {
    public ICollection<int> BuyPacksIDs { get; set; }
    public ICollection<int> BuyProductsIDs { get; set; }

    public double? Price { get; set; }
  }
}
