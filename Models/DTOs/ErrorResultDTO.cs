using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class ErrorResultDTO {
    public string Title { get; set; }
  }
}
