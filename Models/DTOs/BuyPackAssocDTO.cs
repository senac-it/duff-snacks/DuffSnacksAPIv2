using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class BuyPackAssocDTO {
    public int? PackID { get; set; }
    public int? SaleID { get; set; }
  }
}
