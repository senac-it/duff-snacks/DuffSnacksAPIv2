using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class PackAssocAssociateDTO {
    public int PackID { get; set; }

    public int ProductID { get; set; }

    public int Amount { get; set; }
  }
}
