using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class CategoryCreateDTO {
    public string? Name { get; set; }
    public string? Description { get; set; }
  }
}
