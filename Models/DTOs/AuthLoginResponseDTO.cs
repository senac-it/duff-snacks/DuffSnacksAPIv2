using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class AuthLoginResponseDTO {
    public string Token { get; set; }
    public int ID { get; set; }
  }
}
