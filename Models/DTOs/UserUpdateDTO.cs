using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models.DTOs {
  public class UserUpdateDTO {
    public string? Password { get; set; }
    public int? RoleID { get; set; }
  }
}
