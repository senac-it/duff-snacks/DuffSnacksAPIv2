using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class User : BaseModel {
    public string Username { get; set; }
    public string Password { get; set; }

    public int RoleID { get; set; }
    public Role Role { get; set; }
  }
}
