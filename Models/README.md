# Models

Namespace responsável pela criação dos modelos baseado nas tabelas do banco de
dados, sendo utilizado posteriormente no `DbContext` e nas `Migrations`
facilitando o gerênciamento do banco de dados.
