using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class Sale : BaseModel {
    public double Price { get; set; }

    public int UserID { get; set; }
    public User User { get; set; }

    public int ClientID { get; set; }
    public Client Client { get; set; }

    public ICollection<BuyProduct> BuyProducts { get; set; }
    public ICollection<BuyPack> BuyPacks { get; set; }
  }
}
