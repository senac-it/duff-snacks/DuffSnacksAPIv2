using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class Pack : BaseModel {
    [MaxLength(96)]
    public string? Name { get; set; }

    public string? Description { get; set; }

    public bool Disabled { get; set; }

    [DataType(DataType.Currency)]
    public double Price { get; set; }

    public ICollection<PackAssoc>? PacksAssocs { get; set; }
  }
}
