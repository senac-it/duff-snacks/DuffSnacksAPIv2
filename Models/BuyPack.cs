using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class BuyPack : BaseModel {
    public int PackID { get; set; }
    public Pack Pack { get; set; }

    public int SaleID { get; set; }
    public Sale Sale { get; set; }
  }
}
