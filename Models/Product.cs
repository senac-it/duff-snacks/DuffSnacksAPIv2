using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class Product : BaseModel {
    [MaxLength(96)]
    public string? Name { get; set; }

    public string? Description { get; set; }

    [DataType(DataType.Currency)]
    public double Price { get; set; }

    public double Discount { get; set; }

    public bool DiscountIsPercentage { get; set; }

    [Required]
    public int CategoryID { get; set; }

    public Category? Category { get; set; }
  }
}
