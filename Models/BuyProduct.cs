using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class BuyProduct : BaseModel {
    public int ProductID { get; set; }
    public Product Product { get; set; }

    public int SaleID { get; set; }
    public Sale Sale { get; set; }
  }
}
