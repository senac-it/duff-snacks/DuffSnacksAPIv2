using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class PackAssoc : BaseModel {
    [Required]
    public int PackID { get; set; }
    public Pack? Pack { get; set; }

    [Required]
    public int ProductID { get; set; }
    public Product? Product { get; set; }

    public int Amount { get; set; }
  }
}
