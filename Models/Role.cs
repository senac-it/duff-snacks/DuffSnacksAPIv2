using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Models {
  public class Role : BaseModel {
    public string Name { get; set; }
    public bool IsPrivileged { get; set; }
  }
}
