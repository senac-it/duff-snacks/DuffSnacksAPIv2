using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class BuyProductsService : BaseService {
    public BuyProductsService(ApplicationDbContext context) : base(context) { }


    private ICollection<T> BulkState<T>(
      ICollection<T> models, EntityState state
    ) where T : BaseModel {
      foreach (var model in models) _context.Entry<T>(model).State = state;

      _context.SaveChanges();

      return models;
    }

    public IActionResult AssociateManyProducts(
      ICollection<BuyProductAssocDTO> data
    ) {
      try {
        var saleID = (int)data.First().SaleID;

        var toRemoveBuyProducts = _context.BuyProducts
          .Where(b => b.SaleID == saleID)
          .ToList();

        var toAddBuyProducts = data
          .Select(d => new BuyProduct {
            SaleID = saleID,
            ProductID = (int)d.ProductID,
          })
          .ToList();

        BulkState(toRemoveBuyProducts, EntityState.Deleted);
        BulkState(toAddBuyProducts, EntityState.Added);

        return _controller.Ok();
      } catch {
        return _controller.NotFound(
          new ErrorResultDTO {
            Title = "Não foi possível adicionar os produtos ao pedido"
          });
      }
    }
  }
}
