using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Providers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class AuthService : BaseService {
    private readonly IPasswordProvider _passwordProvider;
    private readonly TokenService _tokenService;

    public AuthService(
      ApplicationDbContext context,
      IPasswordProvider passwordProvider,
      TokenService tokenService
    ) : base(context) {
      _passwordProvider = passwordProvider;
      _tokenService = tokenService;
    }

    public ActionResult Login(AuthLoginDTO data) {
      try {
        var hashedPassword = _passwordProvider.Hash(data.Password);

        var user = _context.Users
          .Include(u => u.Role)
          .Where(u => !u.IsDeleted)
          .First(u => u.Username == data.Username && u.Password == hashedPassword);

        if (user == null) return _controller.Unauthorized();

        return _controller.Ok(new AuthLoginResponseDTO {
          Token = _tokenService.GenerateToken(user),
          ID = user.ID,
        });
      } catch {
        return _controller.Unauthorized();
      }
    }
  }
}
