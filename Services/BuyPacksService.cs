using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class BuyPacksService : BaseService {
    public BuyPacksService(ApplicationDbContext context) : base(context) { }


    private ICollection<T> BulkState<T>(
      ICollection<T> models, EntityState state
    ) where T : BaseModel {
      foreach (var model in models) _context.Entry<T>(model).State = state;

      _context.SaveChanges();

      return models;
    }

    public IActionResult AssociateManyPacks(
      ICollection<BuyPackAssocDTO> data
    ) {
      try {
        var saleID = (int)data.First().SaleID;

        var toRemoveBuyPacks = _context.BuyPacks
          .Where(b => b.SaleID == saleID)
          .ToList();

        var toAddBuyPacks = data
          .Select(d => new BuyPack {
            SaleID = saleID,
            PackID = (int)d.PackID,
          })
          .ToList();

        BulkState(toRemoveBuyPacks, EntityState.Deleted);
        BulkState(toAddBuyPacks, EntityState.Added);

        return _controller.Ok();
      } catch {
        return _controller.NotFound(
          new ErrorResultDTO {
            Title = "Não foi possível adicionar os produtos ao pedido"
          });
      }
    }
  }
}
