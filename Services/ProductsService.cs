using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class ProductsService : BaseService {
    public ProductsService(ApplicationDbContext context) : base(context) { }

    public ActionResult<ICollection<Product>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      return _controller.Ok(_context.Products
        .AsNoTracking()
        .Include(p => p.Category)
        .Where(p => !p.IsDeleted)
        .Where(p =>
          p.Name.Contains(search) ||
          p.Description.Contains(search) ||
          p.Category.Name.Contains(search))
        .OrderByDescending(c => c.CreatedAt)
        .ToList());
    }

    public ActionResult<Product> GetById(int id) {
      try {
        return _controller.Ok(_context.Products
          .AsNoTracking()
          .Include(p => p.Category)
          .Where(p => !p.IsDeleted)
          .First(p => p.ID == id));
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Produto não encontrado" });
      }
    }

    public ActionResult<Product> CreateOne(ProductCreateDTO data) {
      var currentTime = DateTime.Now;
      var product = new Product {
        Name = data.Name,
        Description = data.Description,
        IsDeleted = false,
        Discount = data.Discount,
        CategoryID = data.CategoryID,
        DiscountIsPercentage = data.DiscountIsPercentage,
        Price = data.Price,
        CreatedAt = currentTime,
        UpdatedAt = currentTime,
      };

      _context.Entry(product).State = EntityState.Added;

      try {
        _context.SaveChanges();
        return _controller.Ok(product);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel criar o produto" });
      }
    }

    public ActionResult<Product> UpdateOne(int id, ProductUpdateDTO data) {
      try {
        var product = _context.Products.First(p => p.ID == id);

        product.Name = ValidValue(data.Name, product.Name);
        product.Description = ValidValue(data.Description, product.Description);
        product.Price = (int)ValidValue(data.Price, product.Price);
        product.Discount = (int)ValidValue(data.Discount, product.Discount);
        product.DiscountIsPercentage = (bool)ValidValue(
          data.DiscountIsPercentage, product.DiscountIsPercentage);
        product.CategoryID = (int)ValidValue(
          data.CategoryID, product.CategoryID);
        product.UpdatedAt = DateTime.Now;

        _context.Entry(product).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(product);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO {
            Title = "Não foi possivel atualizar o produto",
          }
        );
      }
    }

    public ActionResult<Product> DeleteOne(int id) {
      try {
        var product = _context.Products.First(p => p.ID == id);

        product.IsDeleted = true;
        product.UpdatedAt = DateTime.Now;

        _context.Entry(product).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(product);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel apagar o produto" });
      }
    }
  }
}
