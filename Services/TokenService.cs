using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Providers;

using Microsoft.IdentityModel.Tokens;

namespace DuffSnacksAPIv2.Services {
  public class TokenService {
    private readonly ISecretKeyProvider _secretKey;

    public TokenService(ISecretKeyProvider secretKey) {
      _secretKey = secretKey;
    }

    public string GenerateToken(User user) {
      var secret = _secretKey.GetKey();

      var tokenHandler = new JwtSecurityTokenHandler();

      var key = Encoding.ASCII.GetBytes(secret);

      var tokenDescriptor = new SecurityTokenDescriptor {
        Subject = new ClaimsIdentity(new Claim[] {
          new Claim(ClaimTypes.Name, user.Username.ToString()),
          new Claim(ClaimTypes.Role, user.Role.ID.ToString())
        }),
        Expires = DateTime.UtcNow.AddHours(2),
        SigningCredentials = new SigningCredentials(
          new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
      };

      var token = tokenHandler.CreateToken(tokenDescriptor);

      return tokenHandler.WriteToken(token);
    }
  }
}
