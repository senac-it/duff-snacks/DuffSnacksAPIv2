using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class SalesService : BaseService {
    private readonly BuyProductsService _buyProductsService;
    private readonly BuyPacksService _buyPacksService;

    public SalesService(
      ApplicationDbContext context,
      BuyProductsService buyProductsService,
      BuyPacksService buyPacksService
    ) : base(context) {
      _buyProductsService = buyProductsService;
      _buyPacksService = buyPacksService;
    }

    public override void AddController(ControllerBase controller) {
      base.AddController(controller);
      _buyProductsService.AddController(controller);
      _buyPacksService.AddController(controller);
    }

    public ActionResult<ICollection<Sale>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      var sales = _context.Sales
        .AsNoTracking()
        .Include(s => s.Client)
        .Include(s => s.User)
        .Include(s => s.BuyProducts)
          .ThenInclude(b => b.Product)
        .Include(s => s.BuyPacks)
          .ThenInclude(b => b.Pack)
        .Where(s => !s.IsDeleted)
        .Where(s =>
          s.Client.Fullname.Contains(search) ||
          s.User.Username.Contains(search))
        .OrderByDescending(c => c.CreatedAt)
        .ToList();

      foreach (var sale in sales) sale.User.Password = "";

      return _controller.Ok(sales);
    }

    public ActionResult<Sale> GetById(int id) {
      try {
        var sale = _context.Sales
          .AsNoTracking()
          .Include(s => s.Client)
          .Include(s => s.User)
          .Include(s => s.BuyProducts)
            .ThenInclude(b => b.Product)
          .Include(s => s.BuyPacks)
            .ThenInclude(b => b.Pack)
          .Where(p => !p.IsDeleted)
          .First(p => p.ID == id);

        sale.User.Password = "";

        return _controller.Ok(sale);
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Venda não encontrada" });
      }
    }

    public ActionResult<Sale> CreateOne(SaleCreateDTO data) {
      var currentTime = DateTime.Now;
      var sale = new Sale {
        ClientID = data.ClientID,
        UserID = data.UserID,
        Price = data.Price,
        IsDeleted = false,
        CreatedAt = currentTime,
        UpdatedAt = currentTime,
      };

      _context.Entry(sale).State = EntityState.Added;

      try {
        _context.SaveChanges();

        AssociateBuy(data.BuyProductsIDs, data.BuyPacksIDs, sale.ID);

        return _controller.Ok(sale);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel criar a venda" });
      }
    }

    public ActionResult<Sale> UpdateOne(int id, SaleUpdateDTO data) {
      try {
        var sale = _context.Sales.First(p => p.ID == id);

        sale.Price = (double)ValidValue(data.Price, sale.Price);
        sale.UpdatedAt = DateTime.Now;

        _context.Entry(sale).State = EntityState.Modified;

        _context.SaveChanges();

        AssociateBuy(data.BuyProductsIDs, data.BuyPacksIDs, sale.ID);

        return _controller.Ok(sale);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO {
            Title = "Não foi possivel atualizar a venda",
          }
        );
      }
    }

    private void AssociateBuy(
      ICollection<int> buyProducts,
      ICollection<int> buyPacks,
      int saleID
    ) {
      if (buyProducts != null && buyProducts.Count > 0)
        _buyProductsService.AssociateManyProducts(
          buyProducts
            .Select(b => new BuyProductAssocDTO {
              SaleID = saleID,
              ProductID = b,
            })
            .ToList()
        );

      if (buyPacks != null && buyPacks.Count > 0)
        _buyPacksService.AssociateManyPacks(
          buyPacks
            .Select(b => new BuyPackAssocDTO {
              SaleID = saleID,
              PackID = b,
            })
            .ToList()
        );
    }

    public ActionResult<Sale> DeleteOne(int id) {
      try {
        var sale = _context.Sales.First(p => p.ID == id);

        sale.IsDeleted = true;
        sale.UpdatedAt = DateTime.Now;

        _context.Entry(sale).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(sale);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel apagar a venda" });
      }
    }
  }
}
