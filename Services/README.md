# Services

Namespace responsável pelos serviços da aplicação, comumente utilizados para
gerênciar as regras de negócios da aplicação separando-os do `Controller`.
