using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class CategoriesService : BaseService {
    public CategoriesService(ApplicationDbContext context) : base(context) { }

    public ActionResult<ICollection<Category>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      return _controller.Ok(_context.Categories
        .Where(c => !c.IsDeleted)
        .Where(c => c.Name.Contains(search) || c.Description.Contains(search))
        .OrderByDescending(c => c.CreatedAt)
        .ToList());
    }

    public ActionResult<Category> GetById(int id) {
      try {
        return _controller.Ok(_context.Categories
          .AsNoTracking()
          .Where(c => !c.IsDeleted)
          .First(c => c.ID == id));
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Categoria não encontrada" });
      }
    }

    public ActionResult<Category> CreateOne(CategoryCreateDTO data) {
      var currentTime = DateTime.Now;
      var category = new Category {
        Name = data.Name,
        Description = data.Description,
        IsDeleted = false,
        CreatedAt = currentTime,
        UpdatedAt = currentTime,
      };

      _context.Entry(category).State = EntityState.Added;

      try {
        _context.SaveChanges();
        return _controller.Ok(category);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel criar a categoria" });
      }
    }

    public ActionResult<Category> UpdateOne(int id, CategoryUpdateDTO data) {
      try {
        var category = _context.Categories.First(c => c.ID == id);

        category.Name = ValidValue(data.Name, category.Name);
        category.Description = ValidValue(data.Description, category.Description);
        category.UpdatedAt = DateTime.Now;

        _context.Entry(category).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(category);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO {
            Title = "Não foi possivel atualizar a categoria",
          }
        );
      }
    }

    public ActionResult<Category> DeleteOne(int id) {
      try {
        var category = _context.Categories.First(c => c.ID == id);

        category.IsDeleted = true;
        category.UpdatedAt = DateTime.Now;

        _context.Entry(category).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(category);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel apagar a categoria" });
      }
    }
  }
}
