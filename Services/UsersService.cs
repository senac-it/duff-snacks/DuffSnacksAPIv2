using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Providers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class UsersService : BaseService {
    private readonly IPasswordProvider _passwordProvider;

    public UsersService(
      ApplicationDbContext context,
      IPasswordProvider passwordProvider
    ) : base(context) {
      _passwordProvider = passwordProvider;
    }

    public ActionResult<ICollection<User>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      return _controller.Ok(
        _context.Users
          .AsNoTracking()
          .Include(u => u.Role)
          .Where(u => !u.IsDeleted)
          .Where(u =>
            u.Username.Contains(search) || u.Role.Name.Contains(search))
          .OrderByDescending(c => c.CreatedAt)
          .ToList()
      );
    }

    public ActionResult<User> GetById(int id) {
      try {
        return _controller.Ok(_context.Users
          .AsNoTracking()
          .Include(u => u.Role)
          .Where(c => !c.IsDeleted)
          .First(c => c.ID == id));
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Usuário não encontrado" });
      }
    }

    public ActionResult<User> CreateOne(UserCreateDTO data) {
      var currentTime = DateTime.Now;
      var user = new User {
        Username = data.Username,
        Password = _passwordProvider.Hash(data.Password),
        RoleID = data.RoleID,
        IsDeleted = false,
        CreatedAt = currentTime,
        UpdatedAt = currentTime,
      };

      _context.Entry(user).State = EntityState.Added;

      try {
        _context.SaveChanges();
        return _controller.Ok(user);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel criar o usuário" });
      }
    }

    public ActionResult<User> UpdateOne(int id, UserUpdateDTO data) {
      try {
        var user = _context.Users.First(c => c.ID == id);

        if (ValidValue(data.Password, user.Password) == data.Password)
          user.Password = _passwordProvider.Hash(data.Password);
        user.RoleID = (int)ValidValue(data.RoleID, user.RoleID);
        user.UpdatedAt = DateTime.Now;

        _context.Entry(user).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(user);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO {
            Title = "Não foi possivel atualizar o usuário",
          }
        );
      }
    }

    public ActionResult<User> DeleteOne(int id) {
      try {
        var user = _context.Users.First(c => c.ID == id);

        user.IsDeleted = true;
        user.UpdatedAt = DateTime.Now;

        _context.Entry(user).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(user);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel apagar o usuário" });
      }
    }
  }
}
