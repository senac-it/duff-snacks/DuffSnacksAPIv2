using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;

using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Services {
  public class BaseService {
    protected readonly ApplicationDbContext _context;
    protected ControllerBase _controller;

    public BaseService(ApplicationDbContext context) {
      _context = context;
    }

    public virtual void AddController(ControllerBase controller) {
      _controller = controller;
    }

    protected T ValidValue<T>(T option1, T option2) {
      return option1 != null ? option1 : option2;
    }
  }
}
