using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class PacksAssocsService : BaseService {
    public PacksAssocsService(ApplicationDbContext context) : base(context) { }

    private ICollection<T> BulkState<T>(
    ICollection<T> models, EntityState state) where T : BaseModel {
      foreach (var model in models) _context.Entry<T>(model).State = state;

      _context.SaveChanges();

      return models;
    }

    public IActionResult AssociateMany(
    ICollection<PackAssocAssociateDTO> data) {
      try {
        var packID = data.First().PackID;

        var queryPacksAssocs = _context.PacksAssocs
          .Where(a => a.PackID == packID)
          .ToList();

        var toRemovePacksAssocs = queryPacksAssocs
          .Where(q => !data.Select(d => d.ProductID).Contains(q.ProductID))
          .ToList();

        var currentTime = DateTime.Now;
        var toAddPacksAssocs = data
          .Where(d => !queryPacksAssocs
                        .Select(q => q.ProductID)
                        .Contains(d.ProductID))
          .Select(d => new PackAssoc {
            PackID = packID,
            ProductID = d.ProductID,
            Amount = d.Amount,
            CreatedAt = currentTime,
            UpdatedAt = currentTime,
          })
          .ToList();

        var toUpdatePacksAssocs = queryPacksAssocs
          .Where(q => data.Select(d => d.ProductID).Contains(q.ProductID))
          .ToList();

        BulkState(toUpdatePacksAssocs, EntityState.Modified);

        BulkState(toRemovePacksAssocs, EntityState.Deleted);

        BulkState(toAddPacksAssocs, EntityState.Added);

        return _controller.Ok();
      } catch {
        return _controller.NotFound(
          new ErrorResultDTO {
            Title = "Não foi possível adicionar os produtos ao pacote"
          });
      }
    }
  }
}
