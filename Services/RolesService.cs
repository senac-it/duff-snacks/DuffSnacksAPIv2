using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class RolesService : BaseService {
    public RolesService(ApplicationDbContext context) : base(context) { }

    public ActionResult<ICollection<Role>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      return _controller.Ok(_context.Roles
        .AsNoTracking()
        .Where(c => c.Name.Contains(search))
        .ToList());
    }

    public ActionResult<Role> GetById(int id) {
      try {
        return _controller.Ok(_context.Roles
          .AsNoTracking()
          .First(c => c.ID == id));
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Categoria não encontrada" });
      }
    }
  }
}
