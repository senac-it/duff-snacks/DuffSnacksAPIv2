using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class ClientsService : BaseService {
    public ClientsService(ApplicationDbContext context) : base(context) { }

    public ActionResult<ICollection<Client>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      return _controller.Ok(_context.Clients
        .AsNoTracking()
        .Where(c => !c.IsDeleted)
        .Where(c => c.Fullname.Contains(search) ||
                    c.Address.Contains(search) ||
                    c.Document.Contains(search) ||
                    c.Email.Contains(search))
        .OrderByDescending(c => c.CreatedAt)
        .ToList());
    }

    public ActionResult<Client> GetById(int id) {
      try {
        return _controller.Ok(_context.Clients
          .AsNoTracking()
          .Where(c => !c.IsDeleted)
          .First(c => c.ID == id));
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Cliente não encontrado" });
      }
    }

    public ActionResult<Client> CreateOne(ClientCreateDTO data) {
      var currentTime = DateTime.Now;
      var client = new Client {
        Fullname = data.Fullname,
        Document = data.Document,
        Email = data.Email,
        Address = data.Address,
        IsDeleted = false,
        CreatedAt = currentTime,
        UpdatedAt = currentTime,
      };

      _context.Entry(client).State = EntityState.Added;

      try {
        _context.SaveChanges();
        return _controller.Ok(client);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel criar o cliente" });
      }
    }

    public ActionResult<Client> UpdateOne(int id, ClientUpdateDTO data) {
      try {
        var client = _context.Clients.First(c => c.ID == id);

        client.Fullname = ValidValue(data.Fullname, client.Fullname);
        client.Document = ValidValue(data.Document, client.Document);
        client.Email = ValidValue(data.Email, client.Email);
        client.Address = ValidValue(data.Address, client.Address);
        client.UpdatedAt = DateTime.Now;

        _context.Entry(client).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(client);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO {
            Title = "Não foi possivel atualizar o cliente",
          }
        );
      }
    }

    public ActionResult<Client> DeleteOne(int id) {
      try {
        var client = _context.Clients.First(c => c.ID == id);

        client.IsDeleted = true;
        client.UpdatedAt = DateTime.Now;

        _context.Entry(client).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(client);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel apagar o cliente" });
      }
    }
  }
}
