using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Services {
  public class PacksService : BaseService {
    private readonly PacksAssocsService _packsAssocsService;

    public PacksService(
      ApplicationDbContext context,
      PacksAssocsService packsAssocsService
    ) : base(context) {
      _packsAssocsService = packsAssocsService;
    }

    public override void AddController(ControllerBase controller) {
      base.AddController(controller);
      _packsAssocsService.AddController(controller);
    }

    public ActionResult<ICollection<Pack>> FindAll(string? keyword) {
      var search = $"{keyword?.Trim()}";

      return _controller.Ok(_context.Packs
        .AsNoTracking()
        .Include(p => p.PacksAssocs)
          .ThenInclude(a => a.Product)
        .Where(p => !p.IsDeleted)
        .Where(p =>
          p.Name.Contains(search) ||
          p.Description.Contains(search) ||
          p.PacksAssocs.Any(a => a.Product.Name.Contains(search)))
        .OrderByDescending(c => c.CreatedAt)
        .ToList());
    }

    public ActionResult<Pack> GetById(int id) {
      try {
        return _controller.Ok(_context.Packs
          .AsNoTracking()
          .Include(p => p.PacksAssocs)
            .ThenInclude(a => a.Product)
          .Where(p => !p.IsDeleted)
          .First(p => p.ID == id));
      } catch (InvalidOperationException) {
        return _controller.NotFound(
          new ErrorResultDTO { Title = "Pacote não encontrado" });
      }
    }

    public ActionResult<Pack> CreateOne(PackCreateDTO data) {
      var currentTime = DateTime.Now;
      var pack = new Pack {
        Name = data.Name,
        Description = data.Description,
        Disabled = data.Disabled,
        Price = data.Price,
        IsDeleted = false,
        CreatedAt = currentTime,
        UpdatedAt = currentTime,
      };

      _context.Entry(pack).State = EntityState.Added;

      try {
        _context.SaveChanges();

        _packsAssocsService.AssociateMany(
          data.PacksAssocs
            .Select(a => new PackAssocAssociateDTO {
              PackID = pack.ID,
              ProductID = a.ProductID,
              Amount = a.Amount
            })
            .ToList()
        );

        return _controller.Ok(pack);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel criar o pacote" });
      }
    }

    public ActionResult<Pack> UpdateOne(int id, PackUpdateDTO data) {
      try {
        var pack = _context.Packs.First(p => p.ID == id);

        pack.Name = ValidValue(data.Name, pack.Name);
        pack.Description = ValidValue(data.Description, pack.Description);
        pack.Price = (int)ValidValue(data.Price, pack.Price);
        pack.Disabled = (bool)ValidValue(data.Disabled, pack.Disabled);
        pack.UpdatedAt = DateTime.Now;

        _context.Entry(pack).State = EntityState.Modified;

        _context.SaveChanges();

        if (data.PacksAssocs != null && data.PacksAssocs.Count > 0)
          _packsAssocsService.AssociateMany(
            data.PacksAssocs
              .Select(a => new PackAssocAssociateDTO {
                Amount = a.Amount,
                PackID = pack.ID,
                ProductID = a.ProductID,
              })
              .ToList()
          );

        return _controller.Ok(pack);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO {
            Title = "Não foi possivel atualizar o pacote",
          }
        );
      }
    }

    public ActionResult<Pack> DeleteOne(int id) {
      try {
        var pack = _context.Packs.First(p => p.ID == id);

        pack.IsDeleted = true;
        pack.UpdatedAt = DateTime.Now;

        _context.Entry(pack).State = EntityState.Modified;

        _context.SaveChanges();
        return _controller.Ok(pack);
      } catch {
        return _controller.BadRequest(
          new ErrorResultDTO { Title = "Não foi possivel apagar o pacote" });
      }
    }
  }
}
