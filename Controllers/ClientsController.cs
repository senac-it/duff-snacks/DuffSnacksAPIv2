using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize]
  public class ClientsController : ControllerBase {
    private readonly ClientsService _clientsService;

    public ClientsController(ClientsService clientsService) {
      _clientsService = clientsService;
      _clientsService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<Client>> GetClients(
    [FromQuery] string? keyword) {
      return _clientsService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<Client> GetClient(int id) {
      return _clientsService.GetById(id);
    }

    [HttpPost]
    public ActionResult<Client> PostClient(ClientCreateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _clientsService.CreateOne(data);
    }

    [HttpPut("{id}")]
    public ActionResult<Client> PutClient(int id, ClientUpdateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _clientsService.UpdateOne(id, data);
    }

    [HttpDelete("{id}")]
    public ActionResult<Client> DeleteClientById(int id) {
      return _clientsService.DeleteOne(id);
    }
  }
}
