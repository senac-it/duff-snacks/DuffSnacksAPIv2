using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize]
  public class SalesController : ControllerBase {
    private readonly SalesService _salesService;

    public SalesController(SalesService salesService) {
      _salesService = salesService;
      _salesService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<Sale>> GetSales(
    [FromQuery] string? keyword) {
      return _salesService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<Sale> GetSale(int id) {
      return _salesService.GetById(id);
    }

    [HttpPost]
    public ActionResult<Sale> PostSale(SaleCreateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _salesService.CreateOne(data);
    }

    [HttpPut("{id}")]
    public ActionResult<Sale> PutSale(int id, SaleUpdateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _salesService.UpdateOne(id, data);
    }

    [HttpDelete("{id}")]
    public ActionResult<Sale> DeleteSale(int id) {
      return _salesService.DeleteOne(id);
    }
  }
}
