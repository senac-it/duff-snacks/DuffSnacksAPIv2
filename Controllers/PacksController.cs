using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize]
  public class PacksController : ControllerBase {
    private readonly PacksService _packsService;

    public PacksController(PacksService packsService) {
      _packsService = packsService;
      _packsService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<Pack>> GetPacks(
    [FromQuery] string? keyword) {
      return _packsService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<Pack> GetPack(int id) {
      return _packsService.GetById(id);
    }

    [HttpPost]
    public ActionResult<Pack> PostPack(PackCreateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _packsService.CreateOne(data);
    }

    [HttpPut("{id}")]
    public ActionResult<Pack> PutPack(int id, PackUpdateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _packsService.UpdateOne(id, data);
    }

    [HttpDelete("{id}")]
    public ActionResult<Pack> DeletePack(int id) {
      return _packsService.DeleteOne(id);
    }
  }
}
