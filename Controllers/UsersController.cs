using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize(Roles = "1")]
  public class UsersController : ControllerBase {
    private readonly UsersService _usersService;

    public UsersController(UsersService usersService) {
      _usersService = usersService;
      _usersService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<User>> GetUsers(
    [FromQuery] string? keyword) {
      return _usersService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<User> GetUser(int id) {
      return _usersService.GetById(id);
    }

    [HttpPost]
    public ActionResult<User> PostUser(UserCreateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _usersService.CreateOne(data);
    }

    [HttpPut("{id}")]
    public ActionResult<User> PutUser(int id, UserUpdateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _usersService.UpdateOne(id, data);
    }

    [HttpDelete("{id}")]
    public ActionResult<User> DeleteUserById(int id) {
      return _usersService.DeleteOne(id);
    }
  }
}
