using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Validators;

using FluentValidation;

using Microsoft.AspNetCore.Mvc;
using DuffSnacksAPIv2.Services;
using Microsoft.AspNetCore.Authorization;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize]
  public class CategoriesController : ControllerBase {
    private readonly CategoriesService _categoriesService;

    public CategoriesController(CategoriesService categoriesService) {
      _categoriesService = categoriesService;
      _categoriesService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<Category>> GetCategories(
    [FromQuery] string? keyword) {
      return _categoriesService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<Category> GetCategory(int id) {
      return _categoriesService.GetById(id);
    }

    [HttpPost]
    public ActionResult<Category> PostCategory(CategoryCreateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _categoriesService.CreateOne(data);
    }

    [HttpPut("{id}")]
    public ActionResult<Category> PutCategory(int id, CategoryUpdateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _categoriesService.UpdateOne(id, data);
    }

    [HttpDelete("{id}")]
    public ActionResult<Category> DeleteCategoryById(int id) {
      return _categoriesService.DeleteOne(id);
    }
  }
}
