# Controllers

Nesse namespace ficam todos os controllers, que são classes com heranças
especificas para a criação de endpoints

A maioria dos controllers aqui tem funcionalidades de CRUD.

## Endpoints

Endpoints são rotas de acesso à API, utilizados para enviar requisições de
recursos do servidor.

### GET, POST, PUT, DELETE `/Categories`

CRUD de categorias

### GET, POST, PUT, DELETE `/Products`

CRUD de produtos

### GET, POST, PUT, DELETE `/Packs`

CRUD de pacotes

### GET, POST, PUT, DELETE `/Clients`

CRUD de clientes

### GET, POST, PUT, DELETE `/Users`

CRUD de usuários

### GET `/Roles`

Apenas leitura de pacotes

### GET, POST, PUT, DELETE `/Sales`

Apenas leitura de sales

### POST `/Auth/Login`

Apenas leitura de sales
