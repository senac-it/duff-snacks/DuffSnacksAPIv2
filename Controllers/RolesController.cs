using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize]
  public class RolesController : ControllerBase {
    private readonly RolesService _rolesService;

    public RolesController(RolesService rolesService) {
      _rolesService = rolesService;
      _rolesService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<Role>> GetUser([FromQuery] string? keyword) {
      return _rolesService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<Role> GetRole(int id) {
      return _rolesService.GetById(id);
    }
  }
}
