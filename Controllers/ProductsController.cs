using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  [Authorize]
  public class ProductsController : ControllerBase {
    private readonly ProductsService _productsService;

    public ProductsController(ProductsService productsService) {
      _productsService = productsService;
      _productsService.AddController(this);
    }

    [HttpGet]
    public ActionResult<ICollection<Product>> GetProducts(
    [FromQuery] string? keyword) {
      return _productsService.FindAll(keyword);
    }

    [HttpGet("{id}")]
    public ActionResult<Product> GetProduct(int id) {
      return _productsService.GetById(id);
    }

    [HttpPost]
    public ActionResult<Product> PostProduct(ProductCreateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _productsService.CreateOne(data);
    }

    [HttpPut("{id}")]
    public ActionResult<Product> PutProduct(int id, ProductUpdateDTO data) {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      return _productsService.UpdateOne(id, data);
    }

    [HttpDelete("{id}")]
    public ActionResult<Product> DeleteProduct(int id) {
      return _productsService.DeleteOne(id);
    }
  }
}
