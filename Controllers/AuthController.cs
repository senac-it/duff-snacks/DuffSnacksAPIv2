using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DuffSnacksAPIv2.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  public class AuthController : ControllerBase {
    private readonly AuthService _authService;

    public AuthController(AuthService authService) {
      _authService = authService;
      _authService.AddController(this);
    }

    [HttpPost("Login")]
    [AllowAnonymous]
    public ActionResult<AuthLoginResponseDTO> PostAuthLoginDTO(
      AuthLoginDTO data
    ) {
      return _authService.Login(data);
    }
  }
}
