using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Providers;
using DuffSnacksAPIv2.Services;

namespace DuffSnacksAPIv2.Configurations {
  public static class ServicesConfiguration {
    public static void InitializeServices(this IServiceCollection services) {
      services.AddTransient<CategoriesService>((sp) =>
        new CategoriesService(sp.GetRequiredService<ApplicationDbContext>()));

      services.AddTransient<ProductsService>((sp) =>
        new ProductsService(sp.GetRequiredService<ApplicationDbContext>()));

      services.AddTransient<PacksAssocsService>((sp) =>
        new PacksAssocsService(sp.GetRequiredService<ApplicationDbContext>()));

      services.AddTransient<PacksService>((sp) =>
        new PacksService(
          sp.GetRequiredService<ApplicationDbContext>(),
          sp.GetRequiredService<PacksAssocsService>()
        ));

      services.AddTransient<UsersService>((sp) =>
        new UsersService(
          sp.GetRequiredService<ApplicationDbContext>(),
          sp.GetRequiredService<IPasswordProvider>()
        ));

      services.AddTransient<ClientsService>((sp) =>
        new ClientsService(sp.GetRequiredService<ApplicationDbContext>()));

      services.AddTransient<BuyProductsService>((sp) =>
        new BuyProductsService(sp.GetRequiredService<ApplicationDbContext>()));

      services.AddTransient<BuyPacksService>((sp) =>
        new BuyPacksService(sp.GetRequiredService<ApplicationDbContext>()));

      services.AddTransient<SalesService>((sp) =>
        new SalesService(
          sp.GetRequiredService<ApplicationDbContext>(),
          sp.GetRequiredService<BuyProductsService>(),
          sp.GetRequiredService<BuyPacksService>()
        ));

      services.AddScoped<TokenService>(sp =>
        new TokenService(sp.GetRequiredService<ISecretKeyProvider>()));

      services.AddTransient<AuthService>((sp) =>
        new AuthService(
          sp.GetRequiredService<ApplicationDbContext>(),
          sp.GetRequiredService<IPasswordProvider>(),
          sp.GetRequiredService<TokenService>()
        ));

      services.AddTransient<RolesService>((sp) =>
        new RolesService(sp.GetRequiredService<ApplicationDbContext>()));
    }
  }
}
