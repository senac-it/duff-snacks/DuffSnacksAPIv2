# Configurations

Nesse namespace fica localizado todas as configurações de inicialização.

## Serviços

O `ServicesConfiguration` é responsavel por inicializar todos os serviços
utilizados pelos controllers e outros.

O seguinte método é uma extensão a `IServiceCollection`, basta usar o namespace
`DuffSnacksAPIv2.Configurations` no `Program.cs`, por exemplo, e chamar
`builder.Services.InitializeServices()`.

```csharp
public static void InitializeServices(this IServiceCollection services);
```
