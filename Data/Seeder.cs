using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;

using Microsoft.EntityFrameworkCore;
using DuffSnacksAPIv2.Providers;

namespace DuffSnacksAPIv2.Data {
  public class Seeder {
    private readonly ApplicationDbContext _context;
    private readonly IPasswordProvider _passwordProvider;
    private readonly DateTime CurrentTime = DateTime.Now;

    public Seeder(
      ApplicationDbContext context,
      IPasswordProvider passwordProvider
    ) {
      _context = context;
      _passwordProvider = passwordProvider;
    }

    public async void Seed() {
      await _context.Database.EnsureCreatedAsync();

      if (await _context.PacksAssocs.AnyAsync()) return;

      var categories = await CreateCategories();
      var products = await CreateProducts(categories);
      var packs = await CreatePacks();
      var packAssocs = await CreatePacksAssocs(products, packs);
      var roles = await CreateRoles();
      var users = await CreateUsers(roles);
      var clients = await CreateClients();
      var sales = await CreateSales(clients, users);
      var buyProducts = await CreateBuyProducts(products, sales);
      var buyPacks = await CreateBuyPacks(packs, sales);
    }

    private async Task<int> BulkSave<T>(ICollection<T> model) where T : BaseModel {
      foreach (var item in model) {
        item.CreatedAt = CurrentTime;
        item.UpdatedAt = CurrentTime;
        _context.Entry<T>(item).State = EntityState.Added;
      }

      return await _context.SaveChangesAsync();
    }

    private async Task<ICollection<Category>> CreateCategories() {
      var categories = new List<Category> {
        new () { Name = "Salgadinho" },
        new () { Name = "Molho" },
        new () { Name = "Refrigerante" },
        new () { Name = "Energético" },
        new () { Name = "Doce" },
      };

      await BulkSave(categories);

      return categories;
    }

    private async Task<ICollection<Product>> CreateProducts(ICollection<Category> categories) {
      var salgadinho = categories.First(c => c.Name == "Salgadinho");
      var molho = categories.First(c => c.Name == "Molho");
      var refrigerante = categories.First(c => c.Name == "Refrigerante");
      var energetico = categories.First(c => c.Name == "Energético");
      var doce = categories.First(c => c.Name == "Doce");

      var products = new List<Product> {
        new () { Price = 10.0, Name = "Doritos", Category = salgadinho },
        new () { Price = 10.0, Name = "Ruffles", Category = salgadinho },
        new () { Price = 10.0, Name = "Cheetos", Category = salgadinho },
        new () { Price = 10.0, Name = "Fandangos", Category = salgadinho },

        new () { Price = 10.0, Name = "Guacamole", Category = molho },

        new () { Price = 10.0, Name = "Coca-Cola", Category = refrigerante },
        new () { Price = 10.0, Name = "Fanta Uva", Category = refrigerante },
        new () { Price = 10.0, Name = "Fanta Laranja", Category = refrigerante },
        new () { Price = 10.0, Name = "Sprite", Category = refrigerante },

        new () { Price = 10.0, Name = "Monster", Category = energetico },
        new () { Price = 10.0, Name = "Red Bull", Category = energetico },
        new () { Price = 10.0, Name = "Baly", Category = energetico },
        new () { Price = 10.0, Name = "NOS", Category = energetico },

        new () { Price = 10.0, Name = "Fini Bananinhas", Category = doce },
        new () { Price = 10.0, Name = "Fini Minhocas", Category = doce },
        new () { Price = 10.0, Name = "Fini Beijos", Category = doce },
        new () { Price = 10.0, Name = "Fini Tubes", Category = doce },
      };

      await BulkSave(products);

      return products;
    }

    private async Task<ICollection<Pack>> CreatePacks() {
      var packs = new List<Pack> {
        new () { Name = "Pacote 1", Price = 35.5, Description = "Pacote cheio de coisa 1" },
        new () { Name = "Pacote 2", Price = 20.0, Description = "Pacote cheio de coisa 2" },
        new () { Name = "Pacote 3", Price = 50.0, Description = "Pacote cheio de coisa 3" },
        new () { Name = "Pacote 4", Price = 15.7, Description = "Pacote cheio de coisa 4" },
      };

      await BulkSave(packs);

      return packs;
    }

    private async Task<ICollection<PackAssoc>> CreatePacksAssocs(
      ICollection<Product> products, ICollection<Pack> packs) {
      var random = new Random();

      var packAssocs = new List<PackAssoc>();

      for (int i = 0; i < 20; i++)
        packAssocs.Add(new() {
          Product = products.ElementAt(random.Next(products.Count)),
          Pack = packs.ElementAt(random.Next(packs.Count)),
        });

      await BulkSave(packAssocs);

      return packAssocs;
    }

    private async Task<ICollection<Role>> CreateRoles() {
      var Roles = new List<Role> {
        new () { Name = "Administrador", IsPrivileged = true },
        new () { Name = "Vendedor", IsPrivileged = false },
      };

      await BulkSave(Roles);

      return Roles;
    }

    private async Task<ICollection<User>> CreateUsers(ICollection<Role> roles) {
      var admin = roles.ElementAt(0);
      var employee = roles.ElementAt(1);

      var users = new List<User> {
        new () {
          Username = "admin",
          Password = _passwordProvider.Hash("admin"),
          RoleID = admin.ID
        },
        new () {
          Username = "vendedor",
          Password = _passwordProvider.Hash("vendedor"),
          RoleID = employee.ID
        },
      };

      await BulkSave(users);

      return users;
    }

    private async Task<ICollection<Client>> CreateClients() {
      var clients = new List<Client> {
        new () { Fullname = "Walter Washington", Document = "111.111.111-1", Email = "amidatna@lulofag.ao", Address = "Congo - Brazzaville, 912" },
        new () { Fullname = "May Goodwin", Document = "111.111.111-1", Email = "hujjas@dodik.iq", Address = "North Korea, 299" },
        new () { Fullname = "Susie Daniel", Document = "111.111.111-1", Email = "na@ke.net", Address = "Bosnia & Herzegovina, 978" },
        new () { Fullname = "Ivan Abbott", Document = "111.111.111-1", Email = "pipum@ewdop.vi", Address = "Latvia, 270" },
      };

      await BulkSave(clients);

      return clients;
    }

    private async Task<ICollection<Sale>> CreateSales(
      ICollection<Client> clients,
      ICollection<User> users
    ) {
      var random = new Random();

      var sales = new List<Sale>();

      for (int i = 0; i < 20; i++)
        sales.Add(new() {
          Client = clients.ElementAt(random.Next(clients.Count)),
          User = users.ElementAt(random.Next(users.Count)),
          Price = 50 + (random.NextDouble() * 450),
        });

      await BulkSave(sales);

      return sales;
    }

    private async Task<ICollection<BuyProduct>> CreateBuyProducts(
      ICollection<Product> products,
      ICollection<Sale> sales
    ) {
      var random = new Random();

      var buyProducts = new List<BuyProduct>();

      for (int i = 0; i < 20; i++)
        buyProducts.Add(new() {
          Product = products.ElementAt(random.Next(products.Count)),
          Sale = sales.ElementAt(random.Next(sales.Count)),
        });

      await BulkSave(buyProducts);

      return buyProducts;
    }

    private async Task<ICollection<BuyPack>> CreateBuyPacks(
      ICollection<Pack> packs,
      ICollection<Sale> sales
    ) {
      var random = new Random();

      var buyPacks = new List<BuyPack>();

      for (int i = 0; i < 20; i++)
        buyPacks.Add(new() {
          Pack = packs.ElementAt(random.Next(packs.Count)),
          Sale = sales.ElementAt(random.Next(sales.Count)),
        });

      await BulkSave(buyPacks);

      return buyPacks;
    }
  }
}
