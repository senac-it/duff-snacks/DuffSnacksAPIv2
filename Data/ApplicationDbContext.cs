using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models;

using Microsoft.EntityFrameworkCore;

namespace DuffSnacksAPIv2.Data {
  public class ApplicationDbContext : DbContext {
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
      : base(options) { }

    public DbSet<Category> Categories { get; set; }
    public DbSet<Pack> Packs { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<PackAssoc> PacksAssocs { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Client> Clients { get; set; }
    public DbSet<Sale> Sales { get; set; }
    public DbSet<BuyProduct> BuyProducts { get; set; }
    public DbSet<BuyPack> BuyPacks { get; set; }
  }
}
