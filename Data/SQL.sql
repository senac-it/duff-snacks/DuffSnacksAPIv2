﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Categories] (
    [ID] int NOT NULL IDENTITY,
    [Name] nvarchar(96) NULL,
    [Description] nvarchar(max) NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY ([ID])
);
GO

CREATE TABLE [Packs] (
    [ID] int NOT NULL IDENTITY,
    [Name] nvarchar(96) NULL,
    [Description] nvarchar(max) NULL,
    [Disabled] bit NOT NULL,
    [Price] float NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    CONSTRAINT [PK_Packs] PRIMARY KEY ([ID])
);
GO

CREATE TABLE [Products] (
    [ID] int NOT NULL IDENTITY,
    [Name] nvarchar(96) NULL,
    [Description] nvarchar(max) NULL,
    [Price] float NOT NULL,
    [Discount] float NOT NULL,
    [DiscountIsPercentage] bit NOT NULL,
    [CategoryID] int NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_Products_Categories_CategoryID] FOREIGN KEY ([CategoryID]) REFERENCES [Categories] ([ID]) ON DELETE CASCADE
);
GO

CREATE TABLE [PacksAssocs] (
    [ID] int NOT NULL IDENTITY,
    [PackID] int NOT NULL,
    [ProductID] int NOT NULL,
    [Amount] int NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    CONSTRAINT [PK_PacksAssocs] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_PacksAssocs_Packs_PackID] FOREIGN KEY ([PackID]) REFERENCES [Packs] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_PacksAssocs_Products_ProductID] FOREIGN KEY ([ProductID]) REFERENCES [Products] ([ID]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_PacksAssocs_PackID] ON [PacksAssocs] ([PackID]);
GO

CREATE INDEX [IX_PacksAssocs_ProductID] ON [PacksAssocs] ([ProductID]);
GO

CREATE INDEX [IX_Products_CategoryID] ON [Products] ([CategoryID]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230316040345_InitialMigrations', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Products] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);
GO

ALTER TABLE [PacksAssocs] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);
GO

ALTER TABLE [Packs] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);
GO

ALTER TABLE [Categories] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230316042640_AddIsDeletedColumnToTables', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Roles] (
    [ID] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    [IsPrivileged] bit NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY ([ID])
);
GO

CREATE TABLE [Users] (
    [ID] int NOT NULL IDENTITY,
    [Username] nvarchar(max) NOT NULL,
    [Password] nvarchar(max) NOT NULL,
    [RoleID] int NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_Users_Roles_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [Roles] ([ID]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Users_RoleID] ON [Users] ([RoleID]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230326175205_AddUserAndRolesTables', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Clients] (
    [ID] int NOT NULL IDENTITY,
    [Fullname] nvarchar(max) NOT NULL,
    [Email] nvarchar(max) NULL,
    [Document] nvarchar(max) NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Clients] PRIMARY KEY ([ID])
);
GO

CREATE TABLE [Sales] (
    [ID] int NOT NULL IDENTITY,
    [Price] float NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Sales] PRIMARY KEY ([ID])
);
GO

CREATE TABLE [BuyHistories] (
    [ID] int NOT NULL IDENTITY,
    [ProductID] int NOT NULL,
    [PackID] int NOT NULL,
    [ClientID] int NOT NULL,
    [UserID] int NOT NULL,
    [SaleID] int NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_BuyHistories] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_BuyHistories_Clients_ClientID] FOREIGN KEY ([ClientID]) REFERENCES [Clients] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BuyHistories_Packs_PackID] FOREIGN KEY ([PackID]) REFERENCES [Packs] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BuyHistories_Products_ProductID] FOREIGN KEY ([ProductID]) REFERENCES [Products] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BuyHistories_Sales_SaleID] FOREIGN KEY ([SaleID]) REFERENCES [Sales] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BuyHistories_Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [Users] ([ID]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_BuyHistories_ClientID] ON [BuyHistories] ([ClientID]);
GO

CREATE INDEX [IX_BuyHistories_PackID] ON [BuyHistories] ([PackID]);
GO

CREATE INDEX [IX_BuyHistories_ProductID] ON [BuyHistories] ([ProductID]);
GO

CREATE INDEX [IX_BuyHistories_SaleID] ON [BuyHistories] ([SaleID]);
GO

CREATE INDEX [IX_BuyHistories_UserID] ON [BuyHistories] ([UserID]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230328202515_AddClientsSalesBuyHistoriesTables', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [BuyHistories] DROP CONSTRAINT [FK_BuyHistories_Clients_ClientID];
GO

ALTER TABLE [BuyHistories] DROP CONSTRAINT [FK_BuyHistories_Users_UserID];
GO

DROP INDEX [IX_BuyHistories_ClientID] ON [BuyHistories];
GO

DROP INDEX [IX_BuyHistories_UserID] ON [BuyHistories];
GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BuyHistories]') AND [c].[name] = N'ClientID');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [BuyHistories] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [BuyHistories] DROP COLUMN [ClientID];
GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BuyHistories]') AND [c].[name] = N'UserID');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [BuyHistories] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [BuyHistories] DROP COLUMN [UserID];
GO

ALTER TABLE [Sales] ADD [ClientID] int NOT NULL DEFAULT 0;
GO

ALTER TABLE [Sales] ADD [UserID] int NOT NULL DEFAULT 0;
GO

CREATE INDEX [IX_Sales_ClientID] ON [Sales] ([ClientID]);
GO

CREATE INDEX [IX_Sales_UserID] ON [Sales] ([UserID]);
GO

ALTER TABLE [Sales] ADD CONSTRAINT [FK_Sales_Clients_ClientID] FOREIGN KEY ([ClientID]) REFERENCES [Clients] ([ID]) ON DELETE CASCADE;
GO

ALTER TABLE [Sales] ADD CONSTRAINT [FK_Sales_Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [Users] ([ID]) ON DELETE CASCADE;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230328204035_MovedClientAndUserColumnToSaleTable', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [BuyHistories] DROP CONSTRAINT [FK_BuyHistories_Packs_PackID];
GO

ALTER TABLE [BuyHistories] DROP CONSTRAINT [FK_BuyHistories_Products_ProductID];
GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BuyHistories]') AND [c].[name] = N'ProductID');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [BuyHistories] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [BuyHistories] ALTER COLUMN [ProductID] int NULL;
GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BuyHistories]') AND [c].[name] = N'PackID');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [BuyHistories] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [BuyHistories] ALTER COLUMN [PackID] int NULL;
GO

ALTER TABLE [BuyHistories] ADD CONSTRAINT [FK_BuyHistories_Packs_PackID] FOREIGN KEY ([PackID]) REFERENCES [Packs] ([ID]);
GO

ALTER TABLE [BuyHistories] ADD CONSTRAINT [FK_BuyHistories_Products_ProductID] FOREIGN KEY ([ProductID]) REFERENCES [Products] ([ID]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230328235814_NullSafetyOnBuyHistory', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [BuyPacks] (
    [ID] int NOT NULL IDENTITY,
    [PackID] int NOT NULL,
    [SaleID] int NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_BuyPacks] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_BuyPacks_Packs_PackID] FOREIGN KEY ([PackID]) REFERENCES [Packs] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BuyPacks_Sales_SaleID] FOREIGN KEY ([SaleID]) REFERENCES [Sales] ([ID]) ON DELETE CASCADE
);
GO

CREATE TABLE [BuyProducts] (
    [ID] int NOT NULL IDENTITY,
    [ProductID] int NOT NULL,
    [SaleID] int NOT NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_BuyProducts] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_BuyProducts_Products_ProductID] FOREIGN KEY ([ProductID]) REFERENCES [Products] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BuyProducts_Sales_SaleID] FOREIGN KEY ([SaleID]) REFERENCES [Sales] ([ID]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_BuyPacks_PackID] ON [BuyPacks] ([PackID]);
GO

CREATE INDEX [IX_BuyPacks_SaleID] ON [BuyPacks] ([SaleID]);
GO

CREATE INDEX [IX_BuyProducts_ProductID] ON [BuyProducts] ([ProductID]);
GO

CREATE INDEX [IX_BuyProducts_SaleID] ON [BuyProducts] ([SaleID]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230329035951_SeparateBuyHistories', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DROP TABLE [BuyHistories];
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230329043606_RemoveBuyHistoriesTable', N'7.0.4');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Clients] ADD [Address] nvarchar(max) NULL;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20230329050453_AddAddressToClient', N'7.0.4');
GO

COMMIT;
GO

