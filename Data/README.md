# Data

Esse namespace é usado para gerenciar tudo que seja relacionado ao banco de
dados, como conexões ao banco de dados, inicialização do contexto do banco de
dados e Seeders, por exemplo.

## ApplicationDbContext

Essa classe é responsável por criar a sessão do banco de dados, passando os
modelos do banco de dados, que serão utilizados posteriormente junto com o
Entity Framework (ORM).

## Seeder

Essa classe é responsável por popular o banco de dados no modo Debug,
facilitando o desenvolvimento com dados prontos.

Além de estar populando, essa classe também está forçando a criação do banco de
dados e suas tabelas.
