using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Utils {
  public static class RegexPatterns {
    public static string CPF => @"^\d{3}.?\d{3}.?\d{3}\-?\d{2}$";
    public static string CNPJ => @"^(\d{2}.?\d{3}.?\d{3}\/?\d{4}\-?\d{2})$";
  }
}
