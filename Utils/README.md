# Utils

Namespace responsável por providenciar coisas úteis a aplicação, como o
`RegexPatterns` que armazena padrões REGEX que podem ser utilizados como
mascara ou validadores.
