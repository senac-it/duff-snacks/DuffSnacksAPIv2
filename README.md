# DuffSnacksAPIv2

## Table of Contents

- [Configurations](./Configurations/README.md)
- [Controllers](./Controllers/README.md)
- [Data](./Data/README.md)
- [Migrations](./Migrations/README.md)
- [Models](./Models/README.md)
  - [DTOs](./Models/DTOs/README.md)
- [Providers](./Providers/README.md)
- [Services](./Services/README.md)
- [Utils](./Utils/README.md)
- [Validators](./Validators/README.md)

- [DuffSnacksAPIv2](#duffsnacksapiv2)
  - [Dev Setup](#dev-setup)
    - [Dependências](#dependências)
    - [Comandos](#comandos)
    - [Docker](#docker)
  - [Acessando a API](#acessando-a-api)

## Dev Setup

Os seguintes passos são nescessários para executar a aplicação:

### Dependências

- SQL Server (Express ou >= 2016)
- ASPNET RUNTIME 7.0
- DOTNET RUNTIME 7.0
- DOTNET SDK 7.0

### Comandos

Restaurando as dependências:

```bash
dotnet restore
```

Aplicando a string de conexão do banco de dados nas váriaveis de ambiente

**Linux:**

```sh
export ConnectionStrings__LocalDB="\
Server=127.0.0.1,1433;\
Database=duffsnacks;\
User Id=sa;\
Password=1234@abcd;\
TrustServerCertificate=True;"
```

**Windows Powershell:**

```powershell
set "ConnectionStrings__LocalDB=Server=127.0.0.1,1433;Database=duffsnacks;User Id=sa;Password=1234@abcd;TrustServerCertificate=True;"
```

### Docker

Para rodar com Docker, é nescessário ter o Docker Compose instalado.

O seguinte comando irá criar uma build da aplicação e subir o banco de dados,
ambos se comunicando:

```sh
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d --build
```

> **⚠️ AVISO:** As vezes o banco de dados demora mais para subir que a API,
> causando a queda da API. Para corrigir isso basta rodar o mesmo comando
> de novo e só a API será levantada.

## Acessando a API

**URL da API rodando pelo Docker:**

> [`http://localhost:5000/swagger`](http://localhost:5000/swagger)

**URL da API rodando sem o Docker:**

> [`http://localhost:5108/swagger`](http://localhost:5108/swagger)
