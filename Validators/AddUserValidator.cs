using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class AddUserValidator : AbstractValidator<UserCreateDTO> {
    public AddUserValidator(ApplicationDbContext context) {
      RuleFor(x => x.Password)
        .NotEmpty()
          .WithMessage("A senha não pode ser vazia");

      RuleFor(x => x.Username)
        .NotEmpty()
          .WithMessage("O nome de usuário não pode estar vazio")
        .Must(x => !context.Users.Any(u => u.Username == x))
          .WithMessage("Esse usuário já está cadastrado");

      RuleFor(x => x.RoleID)
        .NotEmpty()
          .WithMessage("Um cargo deve ser atribuido ao usuário")
        .Must(x => context.Roles.Any(r => r.ID == x))
          .WithMessage("O cargo selecionado não existe");
    }
  }
}
