using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class AddPackValidator : AbstractValidator<PackCreateDTO> {
    public AddPackValidator(ApplicationDbContext context) {
      RuleFor(x => x.Name)
        .NotEmpty()
          .WithMessage("O nome do pacote não pode estar vazio")
        .MaximumLength(96)
          .WithMessage("O tamanho máximo para o nome de um pacote é 96");

      RuleFor(x => x.Price)
        .NotEmpty()
          .WithMessage("É nescessário especificar o valor do pacote");

      RuleFor(x => x.PacksAssocs)
        .NotEmpty()
          .WithMessage("O pacote deve conter ao menos um produto")
        .Must(x => context.Products.Any(p => x
            .Select(y => y.ProductID)
            .Contains(p.ID)))
          .WithMessage("Alguns dos produtos selecionados não existem no sistema");
    }
  }
}
