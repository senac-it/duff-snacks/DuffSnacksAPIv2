using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class UpdateProductValidator : AbstractValidator<ProductUpdateDTO> {
    public UpdateProductValidator(ApplicationDbContext context) {
      RuleFor(x => x.Name)
        .MaximumLength(96)
          .WithMessage("O tamanho máximo para o nome de um produto é 96");

      RuleFor(x => x.CategoryID)
        .Must(x => x != null ? context.Categories.Any(c => c.ID == x) : true)
          .WithMessage("A categoria selecionada não existe no sistema");
    }
  }
}
