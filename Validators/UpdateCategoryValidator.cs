using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class UpdateCategoryValidator : AbstractValidator<CategoryUpdateDTO> {
    public UpdateCategoryValidator() {
      RuleFor(x => x.Name)
        .MaximumLength(96)
          .WithMessage("O limite máximo para o nome da categoria é 96");
    }
  }
}
