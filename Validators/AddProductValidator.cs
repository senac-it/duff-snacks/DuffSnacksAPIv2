using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class AddProductValidator : AbstractValidator<ProductCreateDTO> {
    public AddProductValidator(ApplicationDbContext context) {
      RuleFor(x => x.Name)
        .NotEmpty()
          .WithMessage("O nome do produto não pode estar vazio")
        .MaximumLength(96)
          .WithMessage("O tamanho máximo para o nome de um produto é 96");

      RuleFor(x => x.Price)
        .NotEmpty()
          .WithMessage("É nescessário especificar o valor do pacote");

      RuleFor(x => x.CategoryID)
        .NotEmpty()
          .WithMessage("É nescessário especificar a categoria do produto")
        .Must(x => context.Categories.Any(c => c.ID == x))
          .WithMessage("A categoria selecionada não existe no sistema");
    }
  }
}
