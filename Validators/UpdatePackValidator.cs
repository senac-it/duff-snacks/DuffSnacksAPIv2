using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class UpdatePackValidator : AbstractValidator<PackUpdateDTO> {
    public UpdatePackValidator() {
      RuleFor(x => x.Name)
        .MaximumLength(96)
          .WithMessage("O tamanho máximo para o nome de um pacote é 96");
    }
  }
}
