using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;
using DuffSnacksAPIv2.Utils;

namespace DuffSnacksAPIv2.Validators {
  public class AddClientValidator : AbstractValidator<ClientCreateDTO> {
    public AddClientValidator() {
      RuleFor(x => x.Email)
        .EmailAddress()
          .WithMessage("O formato do campo de email está incorreto");

      RuleFor(x => x.Fullname)
        .NotEmpty()
          .WithMessage("Um nome deve ser fornecido");

      RuleFor(x => x.Document)
        .Must(x =>
          Regex.IsMatch(x.Trim().ToLower(), RegexPatterns.CPF) ||
          Regex.IsMatch(x.Trim().ToLower(), RegexPatterns.CNPJ))
          .WithMessage("O documento está em formato incorreto, deve ser um CPF ou CNPJ");
    }
  }
}
