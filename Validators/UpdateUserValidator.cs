using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class UpdateUserValidator : AbstractValidator<UserUpdateDTO> {
    public UpdateUserValidator(ApplicationDbContext context) {
      RuleFor(x => x.RoleID)
        .Must(x => x != null ? context.Roles.Any(r => r.ID == x) : true)
          .WithMessage("O cargo selecionado não existe");
    }
  }
}
