using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;
using DuffSnacksAPIv2.Utils;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class UpdateClientValidator : AbstractValidator<ClientUpdateDTO> {
    public UpdateClientValidator() {
      RuleFor(x => x.Email)
        .EmailAddress()
          .WithMessage("O formato do campo de email está incorreto");

      RuleFor(x => x.Document)
        .Must(x => x != null ?
            Regex.IsMatch(x.Trim().ToLower(), RegexPatterns.CPF) ||
            Regex.IsMatch(x.Trim().ToLower(), RegexPatterns.CNPJ)
          : true)
          .WithMessage("O documento está em formato incorreto, deve ser um CPF ou CNPJ");
    }
  }
}
