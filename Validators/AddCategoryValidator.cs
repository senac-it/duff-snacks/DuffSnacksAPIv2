using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class AddCategoryValidator : AbstractValidator<CategoryCreateDTO> {
    public AddCategoryValidator() {
      RuleFor(x => x.Name)
        .NotEmpty()
          .WithMessage("O nome da categoria não pode estar vazio")
        .MaximumLength(96)
          .WithMessage("O limite máximo para o nome da categoria é 96");
    }
  }
}
