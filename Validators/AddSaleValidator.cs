using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class AddSaleValidator : AbstractValidator<SaleCreateDTO> {
    public AddSaleValidator(ApplicationDbContext context) {
      RuleFor(x => x.UserID)
        .NotEmpty()
          .WithMessage("É nescessário selecionar um funcionário")
        .Must(x => context.Users.Any(u => u.ID == x))
          .WithMessage("Funcionário não encontrado");

      RuleFor(x => x.ClientID)
        .NotEmpty()
          .WithMessage("É nescessário selecionar um cliente")
        .Must(x => context.Clients.Any(c => c.ID == x))
          .WithMessage("Cliente não encontrado");

      RuleFor(x => x)
        .Must(x => x.BuyPacksIDs?.Count > 0 || x.BuyProductsIDs?.Count > 0)
          .WithMessage("A venda deve conter ao menos um produto ou um pacote");

      RuleFor(x => x.BuyProductsIDs)
        .Must(x => x.Count > 0 ? context.Products.Any(p => x.Contains(p.ID)) : true)
          .WithMessage("Há produtos que não existem no sistema");

      RuleFor(x => x.BuyPacksIDs)
        .Must(x => x.Count > 0 ? context.Packs.Any(p => x.Contains(p.ID)) : true)
          .WithMessage("Há pacotes que não existem no sistema");
    }
  }
}
