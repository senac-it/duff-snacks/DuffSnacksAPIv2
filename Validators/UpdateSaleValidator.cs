using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DuffSnacksAPIv2.Data;
using DuffSnacksAPIv2.Models.DTOs;

using FluentValidation;

namespace DuffSnacksAPIv2.Validators {
  public class UpdateSaleValidator : AbstractValidator<SaleUpdateDTO> {
    public UpdateSaleValidator(ApplicationDbContext context) {
      RuleFor(x => x)
        .Must(x => x.BuyPacksIDs?.Count > 0 || x.BuyProductsIDs?.Count > 0)
          .WithMessage("A venda deve conter ao menos um produto ou um pacote");

      RuleFor(x => x.BuyProductsIDs)
        .Must(x => x.Count > 0 ? context.Products.Any(p => x.Contains(p.ID)) : true)
          .WithMessage("Há produtos que não existem no sistema");

      RuleFor(x => x.BuyPacksIDs)
        .Must(x => x.Count > 0 ? context.Packs.Any(p => x.Contains(p.ID)) : true)
          .WithMessage("Há pacotes que não existem no sistema");
    }
  }
}
