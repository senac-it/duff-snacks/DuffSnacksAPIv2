# Validators

Namespace responsável pela validação de cada DTO. Sempre que alguma requisição
nescessite de um conteúdo, como em JSON por exemplo, e esse conteúdo é modelado
a partir de um DTO (só os que são passados nesse namespace), os valores serão
validados com o FluentValidation antes de executar qualquer inserção ou
atualização no banco de dados.
