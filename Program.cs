using System.Text.Json.Serialization;

using DuffSnacksAPIv2.Data;

using Microsoft.EntityFrameworkCore;
using FluentValidation;
using FluentValidation.AspNetCore;
using DuffSnacksAPIv2.Validators;
using DuffSnacksAPIv2.Models;
using DuffSnacksAPIv2.Services;
using DuffSnacksAPIv2.Providers;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using DuffSnacksAPIv2.Configurations;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSwaggerGen(c => {
  c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
    Name = "Authorization",
    Type = SecuritySchemeType.ApiKey,
    Scheme = "Bearer",
    BearerFormat = "JWT",
    In = ParameterLocation.Header
  });

  c.AddSecurityRequirement(new OpenApiSecurityRequirement {
    {
      new OpenApiSecurityScheme {
        Reference = new OpenApiReference {
          Type = ReferenceType.SecurityScheme,
          Id = "Bearer",
        }
      },
      new string[] {}
    }
  });
});


builder.Services.AddDbContext<ApplicationDbContext>(options =>
  options.UseSqlServer(builder.Configuration.GetConnectionString("LocalDB")));

var key = Environment.GetEnvironmentVariable("JWT_SECURITY_KEY")
      ?? "n42iHLkRAPw0JkuK5tBbw1EUi30GhV1u";

builder.Services.AddSingleton<ISecretKeyProvider>(sp =>
  new SecretKeyProvider(key));
builder.Services.AddSingleton<IPasswordProvider>(sp =>
  new PasswordProvider(sp.GetRequiredService<ISecretKeyProvider>()));

builder.Services.InitializeServices();

builder.Services
  .AddAuthentication(x => {
    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
  })
  .AddJwtBearer(x => {
    x.RequireHttpsMetadata = false;
    x.SaveToken = true;
    x.RefreshInterval = new TimeSpan(12, 0, 0);
    x.TokenValidationParameters = new TokenValidationParameters {
      ValidateIssuerSigningKey = true,
      IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
      ValidateIssuer = false,
      ValidateAudience = false
    };
  });

builder.Services
  .AddControllers()
  .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddValidatorsFromAssemblyContaining<AddProductValidator>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
  app.UseSwagger();
  app.UseSwaggerUI();

  var appServices = app.Services.CreateScope();

  var seeder = new Seeder(
    appServices.ServiceProvider.GetRequiredService<ApplicationDbContext>(),
    appServices.ServiceProvider.GetRequiredService<IPasswordProvider>());
  seeder.Seed();
}

app.UseCors((c) => {
  c.AllowAnyHeader();
  c.AllowAnyMethod();
  c.AllowAnyOrigin();
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
