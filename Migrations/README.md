# Migrations

Esse namespace é gerado automaticamente, utilizado apenas para gerênciar as
versões do banco de dados baseado nos `Models`.

Utilizando o comando `dotnet ef database update` cada migração será executada
uma por uma aplicando as alterações ou atualizando o banco de dados.
