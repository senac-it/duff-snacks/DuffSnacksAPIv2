﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DuffSnacksAPIv2.Migrations
{
    /// <inheritdoc />
    public partial class RemoveBuyHistoriesTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuyHistories");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuyHistories",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PackID = table.Column<int>(type: "int", nullable: true),
                    ProductID = table.Column<int>(type: "int", nullable: true),
                    SaleID = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyHistories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BuyHistories_Packs_PackID",
                        column: x => x.PackID,
                        principalTable: "Packs",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_BuyHistories_Products_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Products",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_BuyHistories_Sales_SaleID",
                        column: x => x.SaleID,
                        principalTable: "Sales",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuyHistories_PackID",
                table: "BuyHistories",
                column: "PackID");

            migrationBuilder.CreateIndex(
                name: "IX_BuyHistories_ProductID",
                table: "BuyHistories",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_BuyHistories_SaleID",
                table: "BuyHistories",
                column: "SaleID");
        }
    }
}
