﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DuffSnacksAPIv2.Migrations
{
    /// <inheritdoc />
    public partial class AddAddressToClient : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Clients",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Clients");
        }
    }
}
