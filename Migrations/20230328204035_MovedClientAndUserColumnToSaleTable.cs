﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DuffSnacksAPIv2.Migrations
{
    /// <inheritdoc />
    public partial class MovedClientAndUserColumnToSaleTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyHistories_Clients_ClientID",
                table: "BuyHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_BuyHistories_Users_UserID",
                table: "BuyHistories");

            migrationBuilder.DropIndex(
                name: "IX_BuyHistories_ClientID",
                table: "BuyHistories");

            migrationBuilder.DropIndex(
                name: "IX_BuyHistories_UserID",
                table: "BuyHistories");

            migrationBuilder.DropColumn(
                name: "ClientID",
                table: "BuyHistories");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "BuyHistories");

            migrationBuilder.AddColumn<int>(
                name: "ClientID",
                table: "Sales",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Sales",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Sales_ClientID",
                table: "Sales",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_UserID",
                table: "Sales",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Clients_ClientID",
                table: "Sales",
                column: "ClientID",
                principalTable: "Clients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Users_UserID",
                table: "Sales",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Clients_ClientID",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Users_UserID",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Sales_ClientID",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Sales_UserID",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "ClientID",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Sales");

            migrationBuilder.AddColumn<int>(
                name: "ClientID",
                table: "BuyHistories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "BuyHistories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BuyHistories_ClientID",
                table: "BuyHistories",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_BuyHistories_UserID",
                table: "BuyHistories",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyHistories_Clients_ClientID",
                table: "BuyHistories",
                column: "ClientID",
                principalTable: "Clients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuyHistories_Users_UserID",
                table: "BuyHistories",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
