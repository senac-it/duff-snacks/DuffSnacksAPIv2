﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DuffSnacksAPIv2.Migrations
{
    /// <inheritdoc />
    public partial class NullSafetyOnBuyHistory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyHistories_Packs_PackID",
                table: "BuyHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_BuyHistories_Products_ProductID",
                table: "BuyHistories");

            migrationBuilder.AlterColumn<int>(
                name: "ProductID",
                table: "BuyHistories",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "PackID",
                table: "BuyHistories",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyHistories_Packs_PackID",
                table: "BuyHistories",
                column: "PackID",
                principalTable: "Packs",
                principalColumn: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyHistories_Products_ProductID",
                table: "BuyHistories",
                column: "ProductID",
                principalTable: "Products",
                principalColumn: "ID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyHistories_Packs_PackID",
                table: "BuyHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_BuyHistories_Products_ProductID",
                table: "BuyHistories");

            migrationBuilder.AlterColumn<int>(
                name: "ProductID",
                table: "BuyHistories",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PackID",
                table: "BuyHistories",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BuyHistories_Packs_PackID",
                table: "BuyHistories",
                column: "PackID",
                principalTable: "Packs",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuyHistories_Products_ProductID",
                table: "BuyHistories",
                column: "ProductID",
                principalTable: "Products",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
