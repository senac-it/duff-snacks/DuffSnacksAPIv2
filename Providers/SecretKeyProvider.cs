using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Providers {
  public class SecretKeyProvider : ISecretKeyProvider {
    private readonly string _key;

    public SecretKeyProvider(string key) => _key = key;

    public string GetKey() => _key;
  }
}
