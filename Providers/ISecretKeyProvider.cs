using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Providers {
  public interface ISecretKeyProvider {
    string GetKey();
  }
}
