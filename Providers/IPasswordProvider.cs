using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Providers {
  public interface IPasswordProvider {
    string Hash(string password);
  }
}
