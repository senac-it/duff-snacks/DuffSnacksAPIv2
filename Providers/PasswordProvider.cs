using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksAPIv2.Providers {
  public class PasswordProvider : IPasswordProvider {
    private readonly ISecretKeyProvider _secretKeyProvider;

    public PasswordProvider(ISecretKeyProvider secretKeyProvider) {
      _secretKeyProvider = secretKeyProvider;
    }

    public string Hash(string password) {
      byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

      HashAlgorithm hashAlgorithm = SHA256.Create();

      var token = Encoding.ASCII.GetBytes(_secretKeyProvider.GetKey());

      byte[] inputBytes = new byte[passwordBytes.Length + token.Length];
      Array.Copy(passwordBytes, 0, inputBytes, 0, passwordBytes.Length);
      Array.Copy(token, 0, inputBytes, passwordBytes.Length, token.Length);

      byte[] hash = hashAlgorithm.ComputeHash(inputBytes);
      string hashedPassword = Convert.ToBase64String(hash);
      return hashedPassword;
    }
  }
}
